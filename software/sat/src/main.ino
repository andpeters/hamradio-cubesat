#define rfTransmitPin 4  
#define ledPin 13        

void setup(){
	pinMode(rfTransmitPin, OUTPUT);     
	pinMode(ledPin, OUTPUT);    
}

void loop(){
	for(int i=4000; i>5; i=i-(i/3)){
		digitalWrite(rfTransmitPin, HIGH);     //HIGH Signal Senden
		digitalWrite(ledPin, HIGH);            //LED Einschalten
		delay(2000);                           //2 Sekunden warten
     
		digitalWrite(rfTransmitPin,LOW);      //LOW Signal Senden
		digitalWrite(ledPin, LOW);            //LED Ausschalten
		delay(i);
	}
}